#include <iostream>
using namespace std;
#ifndef LISTA_H
#define LISTA_H
/* Se defina la estructura "Nodo" */
typedef struct Nodo{
	string dato = "\0";
    struct Nodo *siguiente;
} Nodo;
/* Se define la clase "Lista" */
class Lista{
    /* Atriburos privados de la clase "Lista" */
    private:
        /* Atributos de tipo "Nodo" */
        Nodo *primero = NULL;
        Nodo *ultimo = NULL;
    /* Métodos publicos de la clase "Lista" */
    public:
        /* Constructor por defecto */
        Lista();
        /* Método crear, este crea, liga y ordena los nuevos nodos
        que el usuario ingresa a esta lista dinamica ordenada */
        void crear(string palabra);
        /* Método que genera salida con la todalidad de la lista */
        void imprimir();
        /* Método que completa los enteros faltantes de una lista */
};
#endif
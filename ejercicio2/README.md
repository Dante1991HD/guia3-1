# Guía III-I UI - Ejercicio II

## Descripción del programa 
El programa permite la generación de una Lista dinámica ordenada de Nombres, los muestra en pantalla luego de ser ingresados, ordenados alfabeticamente independiente de si se utiliza mayuscula o no.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./programa
```

## Funcionamiento del programa
El programa Generará una lista según el usuario ingrese Nombres.
* En el siguiente menú **Debes ingresar un número entero de los que aparecen, es decir, _1 o 0_ y presionar la tecla **enter**.

```
¿Que es lo que desea hacer?
---------------------------
Agregar Nombre          [1]
Salir                   [0]
---------------------------
Opción: 
```

### Opción 1 - Agregar Nombre 
```
Opción: 1 
```
* Esta opción permite agregar Nombre a la lista, independiente si estan en minusculas o mayusculas los ordena
como se muestra en el siguiente ejemplo.

```
¿Que es lo que desea hacer?
---------------------------
Agregar Nombre          [1]
Salir                   [0]
---------------------------
Opción: 1
Ingrese valor: Pedro

Valores actuales en su lista: 

[Pedro] 

¿Que es lo que desea hacer?
---------------------------
Agregar Nombre          [1]
Salir                   [0]
---------------------------
Opción: 1
Ingrese valor: juan

Valores actuales en su lista: 

[Juan] [Pedro] 

¿Que es lo que desea hacer?
---------------------------
Agregar Nombre          [1]
Salir                   [0]
---------------------------
Opción: 1
Ingrese valor: diego

Valores actuales en su lista: 

[Diego] [Juan] [Pedro] 
```

* Luego mostrará una salida con los valores ya ordenados alfabeticamente.



### Opción 0 - Salir
```
Opción: 0
```
* Esta opción da termino al ciclo principal del programa efectivamente cerrándolo, genera la siguiente salida.

```
Gracias por preferirnos. Hasta pronto!
```

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl

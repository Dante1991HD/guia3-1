# Guía III-I UI

## Descripción
* **El presente repositorio "Guía III-I UI" contiene tres carpetas con los ejercicios de la guía, en cada carpeta existe su propia documentación, tanto en formato de REAME.md como comentarios en el código.**

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl

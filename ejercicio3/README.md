# Guía III-I UI - Ejercicio III

## Descripción del programa 
El programa permite la generación de una Lista dinámica ordenada de Postres (Strings), a estos se les asocia una lista, sin orden particular, de ingredientes, estos tienen nombre y cantidad, puede mostrarlos en pantalla, agregar más, eliminar, consultar sus ingredientes al igual que agregar nuevos o eliminarlos según convenga.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./programa
```

## Funcionamiento del programa
El programa Generará una lista según el usuario ingrese Postres.
* En el siguiente menú **Debes ingresar un número entero de los que aparecen, es decir, _1, 2, 3, 4 o 0_ y presionar la tecla **enter**.

```
¿Que es lo que desea hacer?
-------------------------------------
Mostrar el listado de Postres     [1]
Agregar Postre:                   [2]
Eliminar Postre:                  [3]
Seleccionar postre, más opciones: [4]
Cerrar Programa                   [0]
-------------------------------------
Opción: 
```

### Opción 1 - Mostrar el listado de Postres
```
Opción: 1 
```
* Esta opción permite mostrar en pantalla los postres ya ingresados en orden alfabetico.
```
Mostrar postres: 

Postres actuales en su lista: 

Lista vacía
```
* Al inicio del programa se encontrará vacía, se agrega un postre de ejemplo.
```
[0] Mote con huesillo
```
### Opción 2 - Agregar Postre:
```
Opción: 2 
```
* Esta opción permite agregar un postre, ademas de sus ingredientes, inicialmente se pueden agregar, ademas de la opción que aparece más adelante.
```
Opción: 2
Agregar postre: Mote con huesillo
Ingrese ingrediente: Mote
Ingrese cantidad de: Mote 100 gramos
¿Ingresar otro ingrediente? Si/No si
Ingrese ingrediente: Huesillos 
Ingrese cantidad de: Huesillos  6 huesillos
¿Ingresar otro ingrediente? Si/No no
```
### Opción 3 - Eliminar Postre: 

### Opción 4 - Seleccionar postre, más opciones
* Esta opción permite seleccionar un postre, por número, para luego desplegar un sub-menú.
```
Opción: 4
Seleccionar Postre: 
[0] Mote con huesillo

Ingrese el número del postre: 0
```
* El sub menú da opciones para trabajar con los datos del postre seleccionado.
```
¿Que es lo que desea hacer?
---------------------------
Mostrar sus ingrediente [1]
Agregar Ingredientes    [2]
Eliminar Ingredeintes   [3]
Regresar                [0]
Opción: 1
Receta de: Mote con huesillo
Ingredientes:
Mote 100 gramos
Huesillos  6 huesillos
---------------------------
Mostrar sus ingrediente [1]
Agregar Ingredientes    [2]
Eliminar Ingredeintes   [3]
Regresar                [0]
Opción: 2
Receta de: Mote con huesillo
Ingrese ingrediente: Agua
Ingrese cantidad de: Agua 1 Litro
¿Que es lo que desea hacer?
---------------------------
Mostrar sus ingrediente [1]
Agregar Ingredientes    [2]
Eliminar Ingredeintes   [3]
Regresar                [0]
Opción: 1
Receta de: Mote con huesillo
Ingredientes:
Mote 100 gramos
Huesillos  6 huesillos
Agua 1 Litro

```

### Opción 0 - Salir
Opción: 0
```
* Esta opción da termino al ciclo principal del programa efectivamente cerrándolo, genera la siguiente salida.

```
Gracias por preferirnos. Hasta pronto!
```

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl

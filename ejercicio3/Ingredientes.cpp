#include <iostream>
#include "Ingredientes.h"
using namespace std;
/* Constructor por defecto de la clase, primero y 
    último atributos de clase de tipo estructura nodo, 
    consta de un nombre, en este caso int, y un puntero de la misma estructura */
Ingredientes::Ingredientes() {
    Ingrediente * primero = NULL;
    Ingrediente * ultimo = NULL;
}
/* Método que crea entradas a la lista dinámica ordenada.
    primeramente crea el nodo, le pasa el entero la variable nombre
    y refencia el puntero al puntero con el que ligará y así
    creará un nuevo nodo según el usuario lo requiera */
void Ingredientes::crear() {
    /* Creamos un "Ingrediente" temporal para contener los datos que pasaremos a los
    atributos de clase según ciertas condiciones, si es el primero en la lista
    pasa el "Ingrediente" temporal al atributo, de lo contrario mientras no estemos 
    en el último "Ingrediente" de la lista los poscicionará en orden ascendente */
    Ingrediente *tmp = new Ingrediente;
    string text = "\0";
    cout << "Ingrese ingrediente: ", getline(cin, text);
    tmp -> nombre = text;
    cout << "Ingrese cantidad de: " << text << " ", getline(cin, text);
    tmp -> cantidad = text;
    tmp -> sig = NULL;
    /* Si el valor del primero es NULL significa que es el primero nombre 
    ingresado en en la lista, por lo que no hay con qué compararlo, por 
    lo que se ingresa directamente */
    if (this -> primero == NULL) {
        this -> primero = tmp;
    } else {
        /* De modo contrario se crearán variables temporales para ayudar
        al intercambio de posición, su tipo de nombre es "Ingrediente", estructura
        descrita anteriormente, "actual" contendrá el valor a comparar dentro
        del ciclo e intercambiar con "sig" */
        Ingrediente * actual, * sig;
        sig = this -> primero;
        /* Se conprueba si el entero sig es menor al que se está creando,
        de serlo se cambia la poscición entre el valor actual si el sig*/
        while (sig != NULL) {
            actual = sig;
            sig = sig -> sig;
        }
        /* Se comprueba si el valor del sig es igual al del primero,
        de ser así el primero tomará el valor del "Ingrediente" creado y el sig
        "Ingrediente" el valor del actual*/
        if (sig == this -> primero) {
            this -> primero = tmp;
            this -> primero -> sig = sig;
        } else {
            /* Al ser diferenctes el "Ingrediente" sig tomará el valor
            del que se está creando y el temporal el del sig para seguir
            siendo comparado en el ciclo */
            actual -> sig = tmp;
            tmp -> sig = sig;
        }
    }
}
void Ingredientes::imprimir() {
    /* utiliza variable temporal para recorrer la lista. */
    Ingrediente * tmp = this -> primero;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << (tmp == NULL ? "Lista vacía\n" : "Ingredientes:\n");
    for(int i = 0; (tmp != NULL); i++)  {
        cout << tmp -> nombre << " " << tmp -> cantidad << endl;
        tmp = tmp -> sig;
    }
    cout << endl;
}
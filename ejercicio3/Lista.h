#include <iostream>
#include "Ingredientes.h"
using namespace std;
#ifndef LISTA_H
#define LISTA_H
/* Se defina la estructura "Receta" */

typedef struct _Receta{
	string nombre = "\0";
    struct _Receta *sig;
    Ingredientes *ingrediente;
} Receta;

/* Se define la clase "Lista" */
class Lista{
    /* Atriburos privados de la clase "Lista" */
    private:
        /* Atributos de tipo "Receta" */
        Receta *primero = NULL;
        Receta *ultimo = NULL;
    /* Métodos publicos de la clase "Lista" */
    public:
        /* Constructor por defecto */
        Lista();
        /* Método crear, este crea, liga y ordena los nuevos nodos
        que el usuario ingresa a esta lista dinamica ordenada */
        void crear(string palabra);
        /* Método que genera salida con la todalidad de la lista */
        void imprimir_postres();
        void imprimir_receta(int busqueda);
        void agregar_ingrediente(int busqueda);
        void eliminar_postre(int busqueda);
        /* Método que completa los enteros faltantes de una lista */
};
#endif
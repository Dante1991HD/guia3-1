#include <iostream>
using namespace std;
#ifndef INGREDIENTES_H
#define INGREDIENTES_H
/* Se defina la estructura "Receta" */


typedef struct _Ingrediente{
	string nombre = "\0";
    string cantidad = "\0";
    struct _Ingrediente *sig;
} Ingrediente;

/* Se define la clase "Lista" */
class Ingredientes{
    /* Atriburos privados de la clase "Lista" */
    private:
        /* Atributos de tipo "Receta" */
        Ingrediente *primero = NULL;
        Ingrediente *ultimo = NULL;
    /* Métodos publicos de la clase "Lista" */
    public:
        /* Constructor por defecto */
        Ingredientes();
        /* Método crear, este crea, liga y ordena los nuevos nodos
        que el usuario ingresa a esta lista dinamica ordenada */
        void crear();
        /* Método que genera salida con la todalidad de la lista */
        void imprimir();
        void eliminar_ingredientes(int eliminar);
};
#endif
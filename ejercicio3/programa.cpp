#include <iostream>
using namespace std;

/* clases */
#include "Lista.h"
/* Menú con salidas para que el usuario tome una decisión */
void menu(){
        cout << "¿Que es lo que desea hacer?" << endl;
        cout << "-------------------------------------" << endl;
        cout << "Mostrar el listado de Postres     [1]" << endl;
        cout << "Agregar Postre:                   [2]" << endl;
        cout << "Eliminar Postre:                  [3]" << endl;
        cout << "Seleccionar postre, más opciones: [4]" << endl;
        cout << "Cerrar Programa                   [0]" << endl;
        cout << "-------------------------------------" << endl;
}
void sub_menu(){
        cout << "¿Que es lo que desea hacer?" << endl;
        cout << "---------------------------" << endl;
        cout << "Mostrar sus ingrediente [1]" << endl;
        cout << "Agregar Ingredientes    [2]" << endl;
        cout << "Eliminar Ingredeintes   [3]" << endl;
        cout << "Regresar                [0]" << endl;
}
/* función principal. */
int main (void) {
    /* Se crean las lista a utilizar */
    Lista *lista = new Lista();
    Lista *positivos = new Lista();
    Lista *negativos = new Lista();

    /* Se declara un booleano "on" como verdadero para el buble principal
    del programa*/
    bool on = true;
    /* "opcion" contendrá lo que el usuario quiere hacer, se usa en la
    estructura switch y desencadena un set de instrucciones según el 
    requerimiento */
    string opcion, dato;
    /* Bucle principal del programa, terminará si el usuario ingresa 0 
    en la variable "opcion", esto cambia el valor de "on" a falso */
        while (on) {
        /* Se crea la lista 3 para ser llenda con los datos de las otras 
        2 listas */
        /* Se llama al manú para su muestra al usuario, este selecciona una
        alternativa y esta se ejecuta */ 
        menu();
        cout << "Opción: ", getline(cin, opcion);
        switch (stoi(opcion)) {
        /* La "opcion" 1 agrega 1 valor a la lista 1 y lo muestra en 
        pantalla */
        case 1:
            cout << "Mostrar postres: " << endl;
            cout << endl << "Postres actuales en su lista: " << endl<< endl;
            lista->imprimir_postres();
            cout << endl;
            break;
        case 2:
            cout << "Agregar postre: ", getline(cin, dato);
            dato[0] = toupper(dato[0]);
            lista->crear(dato);
            cout << endl << "Postres actuales en su lista: " << endl<< endl;
            lista->imprimir_postres();
            cout << endl;
            break;
        case 3:
            cout << "Eliminar postre: ";
            lista->imprimir_postres();
            cout << "Número de postre a eliminar: ", getline(cin, dato);
            lista->eliminar_postre(stoi(dato));
            cout << endl << "Postres actuales en su lista: " << endl<< endl;
            lista->imprimir_postres();
            cout << endl;
            break;
        case 4:
            cout << "Seleccionar Postre: " << endl;
            lista->imprimir_postres();
            cout << "Ingrese el número del postre: ", getline(cin, dato);
            sub_menu();
                    cout << "Opción: ", getline(cin, opcion);
            switch (stoi(opcion)) {
                case 1:
                    lista->imprimir_receta(stoi(dato));
                    cout << endl;
                break;

                case 2:
                    lista->agregar_ingrediente(stoi(dato));

                break;

                case 3:

                break;

                case 0:

                break;

                default:
                
                break;
                }
            break;
        case 0:
            /* La "opcion" 0 cambia el valor de verdad de la variable "on" 
            de verdadeo a falso */
            cout << "Gracias por preferirnos. Hasta pronto!" << endl;
            on = false;
            break;
        default: 
            /* default le dice al usuario que cometió un error al 
            ingresar la "opción" */
            cout << "Ingrese una selección valida del menú por favor" << endl;
            break;
        }
    }    
    /* libera memoria */
    delete lista;

    return 0;
}
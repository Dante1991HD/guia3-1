#include <iostream>
#include "Lista.h"
#include "Ingredientes.h"
using namespace std;
/* Constructor por defecto de la clase, primero y 
    último atributos de clase de tipo estructura nodo, 
    consta de un nombre, en este caso int, y un puntero de la misma estructura */
Lista::Lista() {
    Receta * primero = NULL;
    Receta * ultimo = NULL;
}
/* Método que crea entradas a la lista dinámica ordenada.
    primeramente crea el nodo, le pasa el entero la variable nombre
    y refencia el puntero al puntero con el que ligará y así
    creará un nuevo nodo según el usuario lo requiera */
void Lista::crear(string palabra) {
    /* Creamos un "Receta" temporal para contener los datos que pasaremos a los
    atributos de clase según ciertas condiciones, si es el primero en la lista
    pasa el "Receta" temporal al atributo, de lo contrario mientras no estemos 
    en el último "Receta" de la lista los poscicionará en orden ascendente */
    Receta * tmp = new Receta;
    tmp->ingrediente = new Ingredientes();
    bool ingresando = true;
    while(ingresando){
        tmp ->ingrediente->crear();
        string text = "\0";
        cout << "¿Ingresar otro ingrediente? Si/No ", getline(cin, text);
        ingresando = !(text == "No" || text == "no");
    }

    tmp -> nombre = palabra;
    tmp -> sig = NULL;
    /* Si el valor del primero es NULL significa que es el primero nombre 
    ingresado en en la lista, por lo que no hay con qué compararlo, por 
    lo que se ingresa directamente */
    if (this -> primero == NULL) {
        this -> primero = tmp;
    } else {
        /* De modo contrario se crearán variables temporales para ayudar
        al intercambio de posición, su tipo de nombre es "Receta", estructura
        descrita anteriormente, "actual" contendrá el valor a comparar dentro
        del ciclo e intercambiar con "sig" */
        Receta * actual, * sig;
        sig = this -> primero;
        /* Se conprueba si el entero sig es menor al que se está creando,
        de serlo se cambia la poscición entre el valor actual si el sig*/
        while (sig != NULL && sig -> nombre < palabra) {
            actual = sig;
            sig = sig -> sig;
        }
        /* Se comprueba si el valor del sig es igual al del primero,
        de ser así el primero tomará el valor del "Receta" creado y el sig
        "Receta" el valor del actual*/
        if (sig == this -> primero) {
            this -> primero = tmp;
            this -> primero -> sig = sig;
        } else {
            /* Al ser diferenctes el "Receta" sig tomará el valor
            del que se está creando y el temporal el del sig para seguir
            siendo comparado en el ciclo */
            actual -> sig = tmp;
            tmp -> sig = sig;
        }
    }
}
/* Método que genera una salida con el contenido de la lista */
void Lista::imprimir_postres() {
    /* utiliza variable temporal para recorrer la lista. */
    Receta * tmp = this -> primero;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << (tmp == NULL ? "Lista vacía\n" : "");
    for(int i = 0; (tmp != NULL); i++)  {
        cout << "[" << i << "] " << tmp -> nombre << endl;
        tmp = tmp -> sig;
    }
    cout << endl;
}

void Lista::imprimir_receta(int busqueda) {
    /* utiliza variable temporal para recorrer la lista. */
    Receta * tmp = this -> primero;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << (tmp == NULL ? "Lista vacía\n" : "Receta de: ");
    for(int i = 0; (tmp != NULL); i++)  {
        if(i == busqueda){
            cout << tmp -> nombre << endl;
            tmp->ingrediente->imprimir();
        }
        tmp = tmp -> sig;
    }
    cout << endl;
}

void Lista::agregar_ingrediente(int busqueda) {
    /* utiliza variable temporal para recorrer la lista. */
    Receta * tmp = this -> primero;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << (tmp == NULL ? "Lista vacía\n" : "Receta de: ");
    for(int i = 0; (tmp != NULL); i++)  {
        if(i == busqueda){
            cout << tmp -> nombre << endl;
            tmp->ingrediente->crear();}
        tmp = tmp -> sig;
    }
    cout << endl;
}

void Lista::eliminar_postre(int busqueda) {
    Receta * tmp = this -> primero;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << (tmp == NULL ? "Lista vacía\n" : "Receta de: ");
    if(busqueda == 0) {
        this->primero = NULL;
    }
    for(int i = 0; (tmp != NULL); i++)  {
        if(i == busqueda){
            tmp = tmp->sig->sig;
            }
        else{
            tmp = tmp -> sig;
        }
    }
    cout << endl;

}
